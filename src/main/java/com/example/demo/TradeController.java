package com.example.demo;

import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.model.Trade;
import com.example.demo.model.Trade.status;
import com.example.demo.repositories.TradeRepository;

import javax.validation.Valid;

import java.util.Date;
import java.util.Dictionary;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

@RestController
@RequestMapping("/trades")
public class TradeController {
	@Autowired
	private TradeRepository repository;
	static Map<String, Double> stockPrices = new HashMap<String, Double>();
	static {
		//Map<String, Double> stockPrices = new HashMap<String, Double>();
		stockPrices.put("RELIANCE", 2327.80);
		stockPrices.put("SBIN", 200.85);
		stockPrices.put("ASIANPAINT", 2054.45);
		stockPrices.put("HDFCBANK", 1099.40);
		stockPrices.put("ICICIBANK", 371.20);
	}
		  
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public List<Trade> getAllTrades() {
	  return repository.findAll();
	}
	
	@RequestMapping(value = "/addTrades", method = RequestMethod.GET)
	public void addTrades() {
	  Trade trade1 = new Trade(new ObjectId(), new Date(), "SBIN", 1000.0, 2, 500.0, Trade.status.CREATED);
	  repository.save(trade1);
	  Trade trade2 = new Trade(new ObjectId(), new Date(), "RELIANCE", 2000.0, 2, 1500.0, Trade.status.CREATED);
	  repository.save(trade2);
	  Trade trade3 = new Trade(new ObjectId(), new Date(), "HDFCBANK", 3000.0, 2, 2000.0, Trade.status.CREATED);
	  repository.save(trade3);
	}
	
	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public Trade getTradeById(@PathVariable("id") ObjectId id) {
	  return repository.findBy_id(id);
	}
	
	@RequestMapping(value = "/{id}", method = RequestMethod.PUT)
	public void modifyTradeById(@PathVariable("id") ObjectId id, @Valid @RequestBody Trade trade) {
	  trade.set_id(id);
	  repository.save(trade);
	}
	
	@RequestMapping(value = "/", method = RequestMethod.POST)
	public Trade createTrade(@Valid @RequestBody Trade trade) {
	  trade.set_id(ObjectId.get());
	  trade.setDate(new Date());
	  trade.setTradeStatus(Trade.status.CREATED);
	  for (Entry<String, Double> entry : stockPrices.entrySet())   {
		    String key = entry.getKey();
		    if(key.equals(trade.getStockSymbol()))
		    {
		    	Double val = entry.getValue();
		    	trade.setCurrentValue(val);
		    }
		}
	  repository.save(trade);
	  return trade;
	}
	
	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	public void deleteTrade(@PathVariable ObjectId id) {
	  repository.delete(repository.findBy_id(id));
	}
	
	@RequestMapping(value = "/{id}/updateStatus", method = RequestMethod.PUT)
	public ResponseEntity<Object> updateStatus(@PathVariable ObjectId id, @RequestParam String status) {
	   Trade trade = repository.findBy_id(id);
	   if(trade.getTradeStatus().equals(Trade.status.CREATED))
	   {
		   trade.setTradeStatus(Trade.status.valueOf(status));
		   repository.save(trade);
		   return new ResponseEntity<>("Trade status updated successfully", HttpStatus.OK);
	   }
	   else if(trade.getTradeStatus().equals(Trade.status.PENDING))
	   {
		   if(!status.equals("CREATED"))
		   {
			   trade.setTradeStatus(Trade.status.valueOf(status));
			   repository.save(trade);
			   return new ResponseEntity<>("Trade status updated successfully", HttpStatus.OK);
		   }
		   else
		   {
			   return new ResponseEntity<>("Invalid operation", HttpStatus.FORBIDDEN);
		   }
	   }
	   else if(trade.getTradeStatus().equals(Trade.status.PARTIALLY_FILLED))
	   {
		   if(!(status.equals("CREATED") || status.equals("PENDING")))
		   {
			   trade.setTradeStatus(Trade.status.valueOf(status));
			   repository.save(trade);
			   return new ResponseEntity<>("Trade status updated successfully", HttpStatus.OK);
		   }
		   else
		   {
			   return new ResponseEntity<>("Invalid operation", HttpStatus.FORBIDDEN);
		   }
	   }
	   else if(trade.getTradeStatus().equals(Trade.status.FILLED))
	   {
		   if(!(status.equals("CREATED") || status.equals("PENDING") || status.equals("PARTIALLY_FILLED")))
		   {
			   trade.setTradeStatus(Trade.status.valueOf(status));
			   repository.save(trade);
			   return new ResponseEntity<>("Trade status updated successfully", HttpStatus.OK);
		   }
		   else
		   {
			   return new ResponseEntity<>("Invalid operation", HttpStatus.FORBIDDEN);
		   }
	   }
	   else {
		   return new ResponseEntity<>("Invalid operation", HttpStatus.FORBIDDEN);
	   }
//	   repository.save(trade);
//	   return trade;
	}
}
